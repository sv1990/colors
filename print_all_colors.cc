#include "colors/colors.hh"

#include <iostream>

using namespace colors;

#define PRINT_COLOR(col)                                                       \
  std::cout << #col << ": " << (col) << #col << reset_all << '\n'

int main() {
  PRINT_COLOR(bold);
  PRINT_COLOR(dim);
  PRINT_COLOR(underlined);
  PRINT_COLOR(blink);
  PRINT_COLOR(reverse);
  PRINT_COLOR(hidden);

  PRINT_COLOR(black);
  PRINT_COLOR(red);
  PRINT_COLOR(green);
  PRINT_COLOR(yellow);
  PRINT_COLOR(blue);
  PRINT_COLOR(magenta);
  PRINT_COLOR(cyan);
  PRINT_COLOR(light_gray);
  PRINT_COLOR(default_);
  PRINT_COLOR(dark_gray);
  PRINT_COLOR(light_red);
  PRINT_COLOR(light_green);
  PRINT_COLOR(light_yellow);
  PRINT_COLOR(light_blue);
  PRINT_COLOR(light_magenta);
  PRINT_COLOR(light_cyan);
  PRINT_COLOR(white);

  PRINT_COLOR(background_black);
  PRINT_COLOR(background_red);
  PRINT_COLOR(background_green);
  PRINT_COLOR(background_yellow);
  PRINT_COLOR(background_blue);
  PRINT_COLOR(background_magenta);
  PRINT_COLOR(background_cyan);
  PRINT_COLOR(background_light_gray);
  PRINT_COLOR(background_default_);
  PRINT_COLOR(background_dark_gray);
  PRINT_COLOR(background_light_red);
  PRINT_COLOR(background_light_green);
  PRINT_COLOR(background_light_yellow);
  PRINT_COLOR(background_light_blue);
  PRINT_COLOR(background_light_magenta);
  PRINT_COLOR(background_light_cyan);
  PRINT_COLOR(background_white);
}
